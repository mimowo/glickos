package org.glickos.app;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {

  @Test
  public void equals_same() {
    Player p1 = new Player("mwo");
    Player p2 = new Player("mwo");
    assertEquals(p2, p1);
  }

  @Test
  public void equals_different() {
    Player p1 = new Player("mwo");
    Player p2 = new Player("pp");
    Assert.assertNotEquals(p2, p1);
  }

  @Test
  public void isRated_default() {
    Player p = new Player("xyz");
    Assert.assertEquals(false, p.isRated());
  }

}
