package org.glickos.app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class PlayerManagerTest {

  PlayerManager players;

  @Before
  public void init() {
    players = new PlayerManager();
  }

  @Test
  public void getOrCreate_get() {
    Player p = new Player("xyz", 1437, 104.0);
    players.addIfNotPresent(ImmutableList.of(p));
    Player retrieved = players.getOrCreate("xyz");
    Assert.assertEquals("xyz", retrieved.getId());
    Assert.assertEquals(1437, retrieved.getR(), 0.01);
    Assert.assertEquals(104.0, retrieved.getRd(), 0.01);
  }

  @Test
  public void getOrCreate_create() {
    Player retrieved = players.getOrCreate("xyz");
    Assert.assertEquals("xyz", retrieved.getId());
    Assert.assertEquals(1500, retrieved.getR(), 0.01);
    Assert.assertEquals(350.0, retrieved.getRd(), 0.01);
  }

  @Test
  public void addIfNotPresent_alreadyPresent() {
    Player p = new Player("xyz", 1437, 104.0);
    players.addIfNotPresent(p);
    Player p2 = new Player("xyz", 1500, 350.0);
    players.addIfNotPresent(p);
    players.addIfNotPresent(p2);
    Player retrieved = players.getOrCreate("xyz");
    Assert.assertEquals("xyz", retrieved.getId());
    Assert.assertEquals(1437, retrieved.getR(), 0.01);
    Assert.assertEquals(104.0, retrieved.getRd(), 0.01);
  }

}
