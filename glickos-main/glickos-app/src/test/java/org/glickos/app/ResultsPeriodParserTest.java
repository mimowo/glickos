package org.glickos.app;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ResultsPeriodParserTest {

  ResultsPeriodParser parser = new ResultsPeriodParser();

  @Test
  public void parse_simple() {
    List<String> lines = new ArrayList<>();
    lines.add("a,b,2:10,c,d");
    lines.add("a,c,7:10,b,d");
    ResultsPeriod results = parser.parse(lines.stream());
    List<Result> rs = results.getResults();
    Assert.assertEquals(2, rs.size());
  }

  @Test
  public void parse_withEmptyLine() {
    List<String> lines = new ArrayList<>();
    lines.add("a,b,2:10,c,d");
    lines.add("");
    lines.add("a,c,7:10,b,d");
    ResultsPeriod results = parser.parse(lines.stream());
    List<Result> rs = results.getResults();
    Assert.assertEquals(2, rs.size());
  }

  @Test
  public void parse_withComments() {
    List<String> lines = new ArrayList<>();
    lines.add("# Comment line 1");
    lines.add("# Comment line 2");
    lines.add("a,b,2:10,c,d");
    lines.add("# Comment line 3");
    lines.add("a,c,7:10,b,d");
    ResultsPeriod results = parser.parse(lines.stream());
    List<Result> rs = results.getResults();
    Assert.assertEquals(2, rs.size());
  }

  @Test
  public void parse_invalidNumberOfTokens() {
    List<String> lines = new ArrayList<>();
    lines.add("a,b,2:10,c,");
    try {
      parser.parse(lines.stream());
      Assert.fail();
    } catch (RuntimeException e) {
      Assert.assertEquals("Expected 6 tokens: a,b,2:10,c,", e.getMessage());
    }
  }

  @Test
  public void parse_invalidScoreFormat() {
    List<String> lines = new ArrayList<>();
    lines.add("a,b,2:xxx,c,d");
    try {
      parser.parse(lines.stream());
      Assert.fail();
    } catch (RuntimeException e) {
      Assert.assertEquals("For input string: \"xxx\"", e.getMessage());
    }
  }

  @Test
  public void parse_invalidPlayerAgainstHimself() {
    List<String> lines = new ArrayList<>();
    lines.add("a,b,2:10,c,a");
    try {
      parser.parse(lines.stream());
      Assert.fail();
    } catch (RuntimeException e) {
      Assert.assertEquals("Player cannot play against himself: a,b,2:10,c,a", e.getMessage());
    }
  }

}
