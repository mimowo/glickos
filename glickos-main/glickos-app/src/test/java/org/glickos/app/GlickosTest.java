package org.glickos.app;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class GlickosTest {

  Glickos glickos = new Glickos(63.2);

  Player mwo = new Player("mwo");
  Player pp = new Player("pp");
  Player kb = new Player("kb");
  Player mz = new Player("mz");
  Player xxx = new Player("xxx");
  Player x, a, b, c;
  Result ra, rb, rc;
  List<Result> exampleResults;
  List<Result> emptyResults = ImmutableList.of();

  @Before
  public void init() {
    x = new Player("x", 1500, 200);
    a = new Player("a", 1400, 30);
    b = new Player("b", 1550, 100);
    c = new Player("c", 1700, 300);
    ra = new Result(x, x, a, a, 1, 0);
    rb = new Result(x, x, b, b, 0, 1);
    rc = new Result(x, x, c, c, 0, 1);
    exampleResults = ImmutableList.of(ra, rb, rc);
  }

  @Test
  public void getQ() {
    Assert.assertEquals(0.0057565, glickos.getQ(), 0.000001);
  }

  @Test
  public void g() {
    Assert.assertEquals(0.9955, glickos.g(30), 0.001);
    Assert.assertEquals(0.9531, glickos.g(100), 0.001);
    Assert.assertEquals(0.7242, glickos.g(300), 0.001);
  }

  @Test
  public void E() {
    Assert.assertEquals(0.639, glickos.E(x, ra), 0.001);
    Assert.assertEquals(0.432, glickos.E(x, rb), 0.001);
    Assert.assertEquals(0.303, glickos.E(x, rc), 0.001);
  }

  @Test
  public void d2() {
    Assert.assertEquals(53685.742, glickos.d2(x, exampleResults), 0.001);
  }

  @Test
  public void newR() {
    Assert.assertEquals(1464, glickos.newR(x, exampleResults), 0.2);
  }

  @Test
  public void newRd() {
    Assert.assertEquals(151.4, glickos.newRd(x, exampleResults), 0.01);
  }

  @Test
  public void update_oneRound() {
    PlayerManager players = new PlayerManager(x, a, b, c);
    glickos.update(players, exampleResults);
    Assert.assertEquals(1461, x.getR(), 1);
    Assert.assertEquals(157, x.getRd(), 1);
    Assert.assertEquals(1391, a.getR(), 1);
    Assert.assertEquals(1577, b.getR(), 1);
    Assert.assertEquals(1787, c.getR(), 1);
  }

  @Test
  public void update_manyRounds() {
    PlayerManager players = new PlayerManager(x, a, b, c);
    glickos.update(players, exampleResults);
    Assert.assertEquals(1461, x.getR(), 1);
    Assert.assertEquals(157, x.getRd(), 1);
    Assert.assertEquals(1391, a.getR(), 1);
    Assert.assertEquals(69, a.getRd(), 1);
    glickos.update(players, exampleResults);
    Assert.assertEquals(1452, x.getR(), 1);
    Assert.assertEquals(138, x.getRd(), 1);
    Assert.assertEquals(1374, a.getR(), 1);
    Assert.assertEquals(91, a.getRd(), 1);
    glickos.update(players, exampleResults);
    Assert.assertEquals(1449, x.getR(), 1);
    Assert.assertEquals(130, x.getRd(), 1);
    Assert.assertEquals(1351, a.getR(), 1);
    Assert.assertEquals(107, a.getRd(), 1);
  }

  @Test
  public void update_deviationIncreasesWhenNoGamesPlayed() {
    PlayerManager players = new PlayerManager(x, a, b, c);
    glickos.update(players, exampleResults);
    Assert.assertEquals(1461, x.getR(), 1);
    Assert.assertEquals(157, x.getRd(), 1);
    Assert.assertEquals(1391, a.getR(), 1);
    Assert.assertEquals(69, a.getRd(), 1);
    glickos.update(players, emptyResults);
    Assert.assertEquals(1461, x.getR(), 1);
    Assert.assertEquals(169.6, x.getRd(), 1);
    Assert.assertEquals(1391.2, a.getR(), 1);
    Assert.assertEquals(93.6, a.getRd(), 1);
    glickos.update(players, emptyResults);
    Assert.assertEquals(1461, x.getR(), 1);
    Assert.assertEquals(181.03, x.getRd(), 1);
    Assert.assertEquals(1391.2, a.getR(), 1);
    Assert.assertEquals(112.9, a.getRd(), 1);
  }

  @Test
  public void update_diffrentRankingShift() {
    Player p1 = new Player("p1", 1400, 100);
    Player p2 = new Player("p2", 1400, 300);
    Player p3 = new Player("p3", 1500, 200);
    Player p4 = new Player("p4", 1500, 200);
    Result res = new Result(p1, p2, p3, p4, 9, 10);
    PlayerManager players = new PlayerManager(p1, p2, p3, p4);
    glickos.update(players, ImmutableList.of(res));
    Assert.assertEquals(1405.2, p1.getR(), 1);
    Assert.assertEquals(1427.0, p2.getR(), 1);
  }

}
