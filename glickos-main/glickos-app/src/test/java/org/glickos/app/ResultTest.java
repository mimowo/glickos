package org.glickos.app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ResultTest {

  Player mwo = new Player("mwo");
  Player pp = new Player("pp");
  Player kb = new Player("kb");
  Player mz = new Player("mz");
  Player xxx = new Player("xxx");

  Result r1 = new Result(mwo, pp, kb, mz, 3, 7);

  @Before
  public void init() {
  }

  @Test
  public void getPartner() {
    Assert.assertEquals(pp, r1.getPartner(mwo));
    Assert.assertEquals(mwo, r1.getPartner(pp));
    Assert.assertEquals(kb, r1.getPartner(mz));
    Assert.assertEquals(mz, r1.getPartner(kb));
  }

  @Test
  public void getOpponent() {
    Assert.assertEquals(kb, r1.getOpponent1(mwo));
    Assert.assertEquals(mz, r1.getOpponent2(mwo));
    Assert.assertEquals(kb, r1.getOpponent1(pp));
    Assert.assertEquals(mz, r1.getOpponent2(pp));
    Assert.assertEquals(mwo, r1.getOpponent1(kb));
    Assert.assertEquals(pp, r1.getOpponent2(kb));
    Assert.assertEquals(mwo, r1.getOpponent1(mz));
    Assert.assertEquals(pp, r1.getOpponent2(mz));
  }

  @Test
  public void getPoints_forSinglePlayers() {
    Assert.assertEquals(3, r1.getPoints(mwo));
    Assert.assertEquals(3, r1.getPoints(pp));
    Assert.assertEquals(7, r1.getPoints(kb));
    Assert.assertEquals(7, r1.getPoints(mz));
  }

  @Test
  public void getPoints_forPair() {
    Assert.assertEquals(3, r1.getPointsAb());
    Assert.assertEquals(7, r1.getPointsCd());
  }

  @Test
  public void getPlayer() {
    Assert.assertEquals(mwo, r1.getA());
    Assert.assertEquals(pp, r1.getB());
    Assert.assertEquals(kb, r1.getC());
    Assert.assertEquals(mz, r1.getD());
  }

  @Test
  public void getScore_forPlayer() {
    Assert.assertEquals(0.3, r1.getScore(mwo), 0.00001);
    Assert.assertEquals(0.3, r1.getScore(pp), 0.00001);
    Assert.assertEquals(0.7, r1.getScore(kb), 0.00001);
    Assert.assertEquals(0.7, r1.getScore(mz), 0.00001);
  }

  @Test
  public void notPresent() {
    try {
      r1.getPartner(xxx);
    } catch (IllegalArgumentException e) {
      String exp = "Player xxx is not present in this game";
      Assert.assertEquals(exp, e.getMessage());
    }
  }
}
