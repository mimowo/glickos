package org.glickos.app;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;

public class GlickosRunnerTest {

  @Mock
  DataProvider dataProvider;

  @Mock
  RatingOutoutListener ratingListener;

  PlayerManager players = new PlayerManager();
  Glickos glickos = new Glickos();

  @InjectMocks
  GlickosRunner runner = new GlickosRunner(players, dataProvider, glickos, ratingListener);

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void run_singlePeriod() throws IOException {
    Player p1 = new Player("p1");
    Player p2 = new Player("p2");
    Player p3 = new Player("p3");
    Player p4 = new Player("p4");
    Result res = new Result(p1, p2, p3, p4, 9, 10);
    ResultsPeriod period = new ResultsPeriod(ImmutableList.of(res));
    List<ResultsPeriod> results = ImmutableList.of(period);
    Mockito.when(dataProvider.getResults()).thenReturn(results);
    runner.run();
    Assert.assertEquals(1491.463, p1.getR(), 0.01);
    Assert.assertEquals(1491.463, p2.getR(), 0.01);
    Assert.assertEquals(1508.537, p3.getR(), 0.01);
    Assert.assertEquals(1508.537, p4.getR(), 0.01);
    Assert.assertEquals(true, p1.isRated());
  }

}