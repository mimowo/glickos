package org.glickos.app;

import java.text.DecimalFormat;

public class Player {

  private String id;
  private double r;
  private double rd;
  private boolean isRated;

  public Player(String id) {
    this(id, false, 1500, 350);
  }

  public Player(String id, double r, double rd) {
    this(id, true, r, rd);
  }

  public Player(String id, boolean isRated, double r, double rd) {
    this.id = id;
    this.isRated = isRated;
    this.r = r;
    this.rd = rd;
  }

  public String getId() {
    return this.id;
  }

  public boolean isRated() {
    return this.isRated;
  }

  public double getR() {
    return r;
  }

  public void setR(double r) {
    this.r = r;
  }

  public double getRd() {
    return rd;
  }

  public void setRd(double rd) {
    this.rd = rd;
  }

  public void setRated(boolean isRated) {
    this.isRated = isRated;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Player other = (Player) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  @Override
  public String toString() {
    DecimalFormat formatter = new DecimalFormat("#.###");
    return id + " " + formatter.format(r) + " " + formatter.format(rd);
  }

}
