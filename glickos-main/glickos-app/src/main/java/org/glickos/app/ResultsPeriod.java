package org.glickos.app;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResultsPeriod {

  private List<Result> results;

  public ResultsPeriod() {
    this(new ArrayList<>());
  }

  public ResultsPeriod(List<Result> results) {
    this.results = results;
  }

  public void add(Result result) {
    this.results.add(result);
  }

  public List<Result> getResults() {
    return this.results;
  }

  public String toString() {
    return results.stream().map(Result::toString).collect(Collectors.joining("\n"));
  }

}
