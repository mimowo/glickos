package org.glickos.app;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.glickos.app.guice.GlickosModule;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Inject;

public class App {

  @Inject
  private static DataProvider provider;

  @Inject
  private static GlickosRunner runner;

  @Parameter(names = { "--dir", "-d" })
  String dir;

  public static void main(String[] args) throws IOException {
    App main = new App();
    Guice.createInjector(new GlickosModule()).injectMembers(main);
    System.out.println("Dependencies injected");
    List<String> arguments = ImmutableList.copyOf(args);
    System.out.println("Arguments: " + arguments);
    new JCommander(main, arguments.toArray(new String[arguments.size()]));
    main.run();
  }

  private void run() throws IOException {
    Path dirPath = Paths.get(dir);
    provider.initialize(dirPath);
    runner.run();
  }

}
