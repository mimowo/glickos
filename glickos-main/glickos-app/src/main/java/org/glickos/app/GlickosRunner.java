package org.glickos.app;

import java.io.IOException;
import java.util.List;

import com.google.inject.Inject;

public class GlickosRunner {

  private PlayerManager players;
  private DataProvider dataProvider;
  private Glickos glickos;
  private RatingOutoutListener ratingListener;

  @Inject
  public GlickosRunner(PlayerManager players, DataProvider dataProvider, Glickos glickos,
      RatingOutoutListener ratingListener) {
    this.players = players;
    this.dataProvider = dataProvider;
    this.glickos = glickos;
    this.ratingListener = ratingListener;
  }

  public void run() throws IOException {
    List<ResultsPeriod> periods = dataProvider.getResults();
    for (ResultsPeriod period : periods) {
      List<Result> results = period.getResults();
      updatePlayersBeforePeriod(players, results);
      glickos.update(players, results);
      ratingListener.notify(period, players);
      updatePlayersAfterPeriod(players, results);
    }
  }

  private void updatePlayersAfterPeriod(PlayerManager players2, List<Result> results) {
    for (Result r : results) {
      for (Player p : r.getPlayers()) {
        p.setRated(true);
      }
    }
  }

  private void updatePlayersBeforePeriod(PlayerManager players, List<Result> results) {
    for (Result r : results) {
      for (Player p : r.getPlayers()) {
        players.addIfNotPresent(p);
      }
    }
  }

}
