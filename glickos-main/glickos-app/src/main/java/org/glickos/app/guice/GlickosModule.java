package org.glickos.app.guice;

import org.glickos.app.App;
import org.glickos.app.DataProvider;
import org.glickos.app.Glickos;
import org.glickos.app.GlickosRunner;
import org.glickos.app.ResultsPeriodParser;
import org.glickos.app.PlayerManager;
import org.glickos.app.RatingOutoutListener;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

public class GlickosModule extends AbstractModule {

  @Override
  protected void configure() {
    requestStaticInjection(App.class);
    bind(GlickosRunner.class).in(Scopes.SINGLETON);
    bind(ResultsPeriodParser.class).in(Scopes.SINGLETON);
    bind(Glickos.class).in(Scopes.SINGLETON);
    bind(PlayerManager.class).in(Scopes.SINGLETON);
    bind(DataProvider.class).in(Scopes.SINGLETON);
    bind(RatingOutoutListener.class).in(Scopes.SINGLETON);
  }

}
