package org.glickos.app;

import java.util.List;
import java.util.stream.Collectors;

public class Results {

  public static List<Result> select(Player p, List<Result> results) {
    return results.stream().filter(r -> r.isPresent(p)).collect(Collectors.toList());
  }

}
