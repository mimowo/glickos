package org.glickos.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.inject.Inject;

public class DataProvider {

  private ResultsPeriodParser parser;
  private Path dir;

  @Inject
  public DataProvider(ResultsPeriodParser parser) {
    this.parser = parser;
  }

  public void initialize(Path dir) {
    this.dir = dir;
    validateResultsDir(dir);
  }

  public List<ResultsPeriod> getResults() throws IOException {
    List<ResultsPeriod> periods = new ArrayList<>();
    System.out.printf("%s", dir.toString());
    List<String> periodFns = readPeriodFns(dir);
    List<Path> periodPaths = getConsecutivePeriodPaths(dir, periodFns);
    for (Path periodPath : periodPaths) {
      Stream<String> lines = Files.lines(periodPath);
      ResultsPeriod period = parser.parse(lines);
      periods.add(period);
    }
    return periods;
  }

  private List<String> readPeriodFns(Path path) throws IOException {
    Path periods = path.resolve("periods.txt");
    try (Stream<String> lines = Files.lines(periods)) {
      return lines.map(String::trim).filter(line -> !isComment(line)).collect(Collectors.toList());
    }
  }

  private boolean isComment(String line) {
    return line.startsWith("#");
  }

  private List<Path> getConsecutivePeriodPaths(Path path, List<String> periodFns) {
    List<Path> result = new ArrayList<>();
    for (String periodFn : periodFns) {
      result.add(path.resolve(periodFn));
    }
    return result;
  }

  private Path validateResultsDir(Path dirPath) {
    if (!dirPath.toFile().exists()) {
      throw new RuntimeException("This path does not exist: " + dirPath.toAbsolutePath().toString());
    }
    return dirPath;
  }

}
