package org.glickos.app;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.Iterators;

public class PlayerManager {

  private static final Comparator<Player> comparator = Comparator.comparingDouble(p -> -p.getR());
  private Map<String, Player> players;

  public PlayerManager() {
    this.players = new HashMap<>();
  }

  public PlayerManager(Player... ps) {
    this();
    Iterators.forArray(ps).forEachRemaining(p -> players.put(p.getId(), p));
  }

  public Collection<Player> getPlayers() {
    return this.players.values();
  }

  public Player getOrCreate(String id) {
    if (players.containsKey(id)) {
      return players.get(id);
    }
    Player p = new Player(id);
    players.put(id, p);
    return p;
  }

  public void addIfNotPresent(Collection<Player> players) {
    players.forEach(p -> addIfNotPresent(p));
  }

  public void addIfNotPresent(Player p) {
    if (!players.containsKey(p.getId())) {
      players.put(p.getId(), p);
    }
  }

  @Override
  public String toString() {
    Stream<Player> stream = players.values().stream();
    return stream.sorted(comparator).map(Player::toString).collect(Collectors.joining("\n"));
  }

}
