package org.glickos.app;

import java.util.Collection;

import com.google.common.collect.ImmutableSet;

public class Result {

  private Player a;
  private Player b;
  private Player c;
  private Player d;
  private int pointsAb;
  private int pointsCd;

  public Result(Player a, Player b, Player c, Player d, int pointsAb, int pointsCd) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.pointsAb = pointsAb;
    this.pointsCd = pointsCd;
  }

  public Player getA() {
    return a;
  }

  public Player getB() {
    return b;
  }

  public Player getC() {
    return c;
  }

  public Player getD() {
    return d;
  }

  public int getPointsAb() {
    return pointsAb;
  }

  public int getPointsCd() {
    return pointsCd;
  }

  public boolean isPresent(Player p) {
    return getPlayers().contains(p);
  }

  public Collection<Player> getPlayers() {
    return ImmutableSet.of(a, b, c, d);
  }

  public Player getPartner(Player p) {
    validatePlayerInResult(p);
    if (p.equals(a)) {
      return b;
    } else if (p.equals(b)) {
      return a;
    } else if (p.equals(c)) {
      return d;
    }
    return c;
  }

  public Player getOpponent1(Player p) {
    validatePlayerInResult(p);
    if (p.equals(a)) {
      return c;
    } else if (p.equals(b)) {
      return c;
    } else if (p.equals(c)) {
      return a;
    }
    return a;
  }

  public Player getOpponent2(Player p) {
    validatePlayerInResult(p);
    if (p.equals(a)) {
      return d;
    } else if (p.equals(b)) {
      return d;
    } else if (p.equals(c)) {
      return b;
    }
    return b;
  }

  public int getPoints(Player p) {
    validatePlayerInResult(p);
    if (p.equals(a) || p.equals(b)) {
      return pointsAb;
    }
    return pointsCd;
  }

  private void validatePlayerInResult(Player p) {
    if (isPresent(p)) {
      return;
    }
    String message = "Player " + p.getId() + " is not present in this game";
    throw new IllegalArgumentException(message);
  }

  public double getScore(Player p) {
    int total = pointsAb + pointsCd;
    return Integer.valueOf(getPoints(p)).doubleValue() / total;
  }

  @Override
  public String toString() {
    return a.getId() + "," + b.getId() + ":" + c.getId() + "," + d.getId() + " " + pointsAb + ":" + pointsCd;
  }

}
