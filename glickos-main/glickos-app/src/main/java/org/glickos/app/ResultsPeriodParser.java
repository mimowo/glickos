package org.glickos.app;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public class ResultsPeriodParser {

  public ResultsPeriod parse(Stream<String> lines) {
    ResultsPeriod results = new ResultsPeriod();
    lines.forEach(line -> process(line, results));
    return results;
  }

  private void process(String line, ResultsPeriod results) {
    line = line.trim();
    if (line.length() == 0) {
      return;
    }
    if (line.startsWith("#")) {
      return;
    }
    List<String> rawTokens = ImmutableList.copyOf(line.split("[,:]"));
    List<String> tokens = rawTokens.stream().map(String::trim).collect(Collectors.toList());
    if (tokens.size() != 6) {
      throw new RuntimeException("Expected 6 tokens: " + line);
    }
    String aStr = tokens.get(0);
    String bStr = tokens.get(1);
    Integer pointsAb = Integer.valueOf(tokens.get(2));
    Integer pointsCd = Integer.valueOf(tokens.get(3));
    String cStr = tokens.get(4);
    String dStr = tokens.get(5);
    Player a = new Player(aStr);
    Player b = new Player(bStr);
    Player c = new Player(cStr);
    Player d = new Player(dStr);
    Set<Player> ab = ImmutableSet.of(a, b);
    Set<Player> cd = ImmutableSet.of(c, d);
    if (!Sets.intersection(ab, cd).isEmpty()) {
      throw new RuntimeException("Player cannot play against himself: " + line);
    }
    Result result = new Result(a, b, c, d, pointsAb, pointsCd);
    results.add(result);
  }

}
