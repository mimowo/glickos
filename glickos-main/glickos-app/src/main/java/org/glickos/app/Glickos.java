package org.glickos.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Glickos {

  private final double q = Math.log(10) / 400;
  private double c;

  public Glickos() {
    this(63.2);
  }

  public Glickos(double c) {
    this.c = c;
  }

  public void update(PlayerManager players, List<Result> results) {
    step1(players);
    Map<Player, Double> newRs = new HashMap<>();
    Map<Player, Double> newRds = new HashMap<>();
    step2(players, results, newRs, newRds);
    apply(players, newRs, newRds);
  }

  private void apply(PlayerManager players, Map<Player, Double> newRs, Map<Player, Double> newRds) {
    for (Player p : players.getPlayers()) {
      p.setR(newRs.get(p));
      p.setRd(newRds.get(p));
    }
  }

  private void step1(PlayerManager players) {
    players.getPlayers().forEach(p -> step1(p));
  }

  private void step1(Player p) {
    if (p.isRated()) {
      double rd = p.getRd();
      double newRD = Math.min(350, Math.sqrt(rd * rd + c * c));
      p.setRd(newRD);
    } else {
      p.setRd(350);
    }
  }

  private void step2(PlayerManager players, List<Result> periodResults, Map<Player, Double> newRs,
      Map<Player, Double> newRds) {
    for (Player p : players.getPlayers()) {
      List<Result> playerResults = Results.select(p, periodResults);
      step2(p, playerResults, newRs, newRds);
    }
  }

  private void step2(Player p, List<Result> results, Map<Player, Double> newRs, Map<Player, Double> newRds) {
    double d2 = d2(p, results);
    double newR = newR(p, results, d2);
    double newRd = newRd(p, d2);
    newRs.put(p, newR);
    newRds.put(p, newRd);
  }

  double newR(Player p, List<Result> results) {
    double d2 = d2(p, results);
    return newR(p, results, d2);
  }

  double newR(Player p, List<Result> results, double d2) {
    double c = q / (1.0 / (p.getRd() * p.getRd()) + 1.0 / d2);
    double s = 0.0;
    for (Result result : results) {
      Player o1 = result.getOpponent1(p);
      Player o2 = result.getOpponent2(p);
      double gj = g(a(o1.getRd(), o2.getRd()));
      s += gj * (result.getScore(p) - E(p, result));
    }
    return p.getR() + c * s;
  }

  double newRd(Player p, List<Result> results) {
    double d2 = d2(p, results);
    return newRd(p, d2);
  }

  double newRd(Player p, double d2) {
    double t = 1.0 / (p.getRd() * p.getRd()) + 1.0 / d2;
    return Math.sqrt(1.0 / t);
  }

  double d2(Player p, List<Result> results) {
    double s = 0.0;
    for (Result result : results) {
      Player o1 = result.getOpponent1(p);
      Player o2 = result.getOpponent2(p);
      double gj = g(a(o1.getRd(), o2.getRd()));
      double gj2 = Math.pow(gj, 2);
      double Ej = E(p, result);
      s += gj2 * Ej * (1 - Ej);
    }
    return 1.0 / (q * q * s);
  }

  double E(Player p, Result result) {
    Player o1 = result.getOpponent1(p);
    Player o2 = result.getOpponent2(p);
    double rdj = a(o1.getRd(), o2.getRd());
    double r = a(p.getR(), result.getPartner(p).getR());
    double rj = a(result.getOpponent1(p).getR(), result.getOpponent2(p).getR());
    double exp = -g(rdj) * (r - rj) / 400;
    return 1.0 / (1.0 + Math.pow(10.0, exp));
  }

  double g(double x) {
    return 1.0 / Math.sqrt(1 + 3 * x * x * q * q / (Math.PI * Math.PI));
  }

  double a(double x, double y) {
    return (x + y) / 2;
  }

  double getQ() {
    return q;
  }

}
